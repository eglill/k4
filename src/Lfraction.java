
/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
//      Lfraction f = new Lfraction(5, -7);
//      System.out.println(f.pow(0));
//      System.out.println(f.pow(1));
//      System.out.println(f.pow(-1));
//      System.out.println(f.pow(2));
//      System.out.println(f.pow(3));

//      System.out.println(f.pow(0));
//      System.out.println(f.pow(1));
//      System.out.println(f.pow(-1));
//      System.out.println(f.pow(4));
//      System.out.println(f.pow(-4));
//      System.out.println(f.pow(3));
//      System.out.println(f.pow(-3));
//      Lfraction f1;
//      try {
//         f1 = (Lfraction) f.clone();
//
//      } catch (CloneNotSupportedException e) {
//         f1 = new Lfraction(1, -2);
//      }
//      Lfraction f2 = new Lfraction(8, 9);
//      Lfraction f3 = new Lfraction(0, 3);
//      System.out.println(f);
//      System.out.println(f2);
//      System.out.println(f.getNumerator());
//      System.out.println(f.getDenominator());
//      System.out.println(valueOf("1/-2"));
//      System.out.println(f.equals(f1));
//      System.out.println(f.equals(f2));
//      System.out.println(f2.divideBy(f));
//      System.out.println(f.divideBy(f2));
//      System.out.println(f.minus(f2));
//      System.out.println(f3.inverse());

   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      // Used source: https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java

      if (b == 0) {
         throw new IllegalStateException("Denominator \"" + b + "\" can't be zero.");
      }
      long common = scd(a, b);

      if (b < 0) {
         numerator = a / common / -1;
      } else {
         numerator = a / common;
      }
      denominator = Math.abs(b / common);
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() {
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return Long.toString(numerator) + '/' + denominator;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m == null) {
         return false;
      }
      if(!(m instanceof Lfraction)){
         return false;
      }
      final Lfraction f = (Lfraction) m;

      return f.compareTo(this) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      //Kasutatud allikas: https://stackoverflow.com/questions/10164546/correct-implementation-of-hashcode
      final int prime = 31;
      int result = 1;
      result = prime * result + Long.valueOf(numerator).hashCode();
      result = prime * result + Long.valueOf(denominator).hashCode();
      return result;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      long d = this.getDenominator() * m.getDenominator();
      long n1 = this.getNumerator() * m.getDenominator();
      long n2 = m.getNumerator() * this.getDenominator();

      return new Lfraction(n1 + n2, d);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long d = this.getDenominator() * m.getDenominator();
      long n = this.getNumerator() * m.getNumerator();

      return new Lfraction(n, d);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0) {
         throw new IllegalStateException("Can't invert \"" + this.toString() + "\" if numerator is \"0\".");
      }
      return new Lfraction(denominator, numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(numerator / -1, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      return this.plus(m.opposite());
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      try {
         return this.times(m.inverse());
      } catch (IllegalStateException e) {
         throw new IllegalStateException("Can't divide by zero: (" + this + ") / (" + m + ")");
      }
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      long l1 = this.getNumerator() * m.getDenominator();
      long l2 = m.getNumerator() * this.getDenominator();

      if ((this.numerator == m.numerator) &&
      (this.denominator == m.denominator)) {
         return 0;
      } else if (l1 < l2) {
         return -1;
      } else {
         return 1;
      }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator, denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return numerator / denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(numerator - (denominator * integerPart()), denominator);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double) numerator / (double) denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      if (f - (long) f == 0) {
         return new Lfraction((long) f * d, d);
      }
      return new Lfraction((long) f * d + 1L, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      String[] f = s.split("/");
      try {
         return new Lfraction(Long.parseLong(f[0]), Long.parseLong(f[1]));
      } catch (ArrayIndexOutOfBoundsException e) {
         throw new ArrayIndexOutOfBoundsException("String \"" + s + "\" must contain two values.");
      } catch (NumberFormatException e1) {
         throw new NumberFormatException("String \"" + s + "\" isn't constructed against the rules. Example: \"1/2\"");
      }
   }

   public Lfraction pow(int p) {
      switch (p) {
         case 0:
            return new Lfraction(1, 1);
         case 1:
            return this;
         case -1:
            return this.inverse();
         default:
            if (p > 1) {
               return this.times(this.pow(p - 1));
            } else {
               return this.pow(Math.abs(p)).inverse();
            }
      }
   }

   /** Finds the smallest common denominator
    * @param a numerator
    * @param b denominator
    * @return the smallest common denominator
    */
   private static long scd(long a, long b) {
      // Used source: https://stackoverflow.com/questions/6618994/simplifying-fractions-in-java
      return b == 0 ? Math.abs(a) : scd(b, a % b);
   }
}

